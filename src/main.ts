import { createApp, App as Application } from "vue";
import { router, routerHistory } from "./routes";

import App from "./App.vue";

declare global {
  interface Window {
    // h: HTML5History
    h: typeof routerHistory;
    r: typeof router;
    vm: ReturnType<Application["mount"]>;
  }
}

const app = createApp(App);
app.use(router);
window.vm = app.mount("#app");
