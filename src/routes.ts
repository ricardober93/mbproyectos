
import { createRouter, createWebHistory } from 'vue-router';

import Landing from './pages/Landing.vue';



export const routerHistory = createWebHistory();

export const router = createRouter({
    history: routerHistory,
    strict: true,
    routes: [{
        path: '/',
        component: Landing,
        // children: [
        //   {
        //     // UserProfile will be rendered inside User's <router-view>
        //     // when /user/:id/profile is matched
        //     path: 'profile',
        //     component: UserProfile
        //   },
        //   {
        //     // UserPosts will be rendered inside User's <router-view>
        //     // when /user/:id/posts is matched
        //     path: 'posts',
        //     component: UserPosts
        //   }
        // ]
      }]
  });
